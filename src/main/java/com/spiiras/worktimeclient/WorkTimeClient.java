package com.spiiras.worktimeclient;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;

import javax.swing.*;
import java.awt.*;

/**
 * Created by Igor on 24.04.2017.
 */
@SpringBootApplication
public class WorkTimeClient {

    public static void main(String[] args) {
        new SpringApplicationBuilder(WorkTimeClient.class)
                .headless(false)
                .web(true)
                .run(args);
    }
}
