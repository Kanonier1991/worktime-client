package com.spiiras.worktimeclient.services;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;

import java.io.IOException;

/**
 * Created by Igor on 24.04.2017.
 */
public interface HttpService {

    HttpResponse get(HttpGet get) throws IOException;
    HttpResponse post(HttpPost post) throws IOException;
}
