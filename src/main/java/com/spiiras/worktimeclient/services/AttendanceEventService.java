package com.spiiras.worktimeclient.services;

/**
 * Created by Igor on 25.04.2017.
 */
public interface AttendanceEventService {

    void startAttendance();

    void finishAttendance();

    void lockSystem();

    void unlockSystem();
}
