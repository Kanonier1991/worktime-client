package com.spiiras.worktimeclient.services.impl;

import static com.spiiras.worktimeclient.core.Constants.HOST;
import com.spiiras.worktimeclient.services.AttendanceEventService;
import com.spiiras.worktimeclient.services.HttpService;
import org.apache.http.client.methods.HttpPost;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.time.LocalDateTime;

/**
 * Created by Igor on 25.04.2017.
 */
@Service
public class AttendanceEventServiceImpl implements AttendanceEventService {

    private static final String SAVE_ATTENDANCE_URI = HOST + "/attendance/save";
    private static final String UPDATE_ATTENDANCE_URI = HOST + "/attendance/update";
    private static final String LOCK_SYSTEM_URI = HOST + "/attendance/lock_system";
    private static final String UNLOCK_SYSTEM_URI = HOST + "attendance/unlock_system";

    @Autowired
    private HttpService httpService;

    @Override
    public void startAttendance() {
        HttpPost post = new HttpPost(SAVE_ATTENDANCE_URI);
        try {
            System.out.println(httpService.post(post).getStatusLine());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void finishAttendance() {
        HttpPost post = new HttpPost(UPDATE_ATTENDANCE_URI);
        try {
            System.out.println(httpService.post(post).getStatusLine());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void lockSystem() {
        HttpPost post = new HttpPost(LOCK_SYSTEM_URI);
        try {
            System.out.println(httpService.post(post).getStatusLine());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void unlockSystem() {
        HttpPost post = new HttpPost(UNLOCK_SYSTEM_URI);
        try {
            System.out.println(httpService.post(post).getStatusLine());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
