package com.spiiras.worktimeclient.services.impl;

import com.spiiras.worktimeclient.services.AttendanceEventService;
import com.spiiras.worktimeclient.services.HttpService;
import com.spiiras.worktimeclient.services.StarterService;
import org.apache.http.client.methods.HttpPost;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.net.URL;

/**
 * Created by Igor on 24.04.2017.
 */
@Service
public class StarterServiceImpl implements StarterService {

    @Autowired
    private AttendanceEventService attendanceEventService;

    private PopupMenu trayMenu;
    private TrayIcon trayIcon;

    @PostConstruct
    public void activate() {

        attendanceEventService.startAttendance();

        createTrayMenu();

        URL imageURL = getClass().getResource("/clock-200.png");
        ImageIcon icon = new ImageIcon(imageURL);
        trayIcon = new TrayIcon(icon.getImage(), "WTClient", trayMenu);
        trayIcon.setImageAutoSize(true);

        SystemTray tray = SystemTray.getSystemTray();
        try {
            tray.add(trayIcon);
        } catch (AWTException e) {
            e.printStackTrace();
        }

        trayIcon.displayMessage("WTClient", "Приложение запущено!",
                TrayIcon.MessageType.INFO);

        Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {
            @Override
            public void run() {
                attendanceEventService.finishAttendance();
            }
        }));
    }

    @PreDestroy
    public void destroy() {
        attendanceEventService.finishAttendance();
    }

    private void createTrayMenu(){
        trayMenu = new PopupMenu();
        MenuItem exitItem = new MenuItem("Выход");
        exitItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.exit(0);
            }
        });
        trayMenu.add(exitItem);
    }
}
