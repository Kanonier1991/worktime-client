package com.spiiras.worktimeclient.services.impl;

import com.spiiras.worktimeclient.services.HttpService;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.HttpClientBuilder;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.IOException;

/**
 * Created by Igor on 24.04.2017.
 */
@Service
public class HttpServiceImpl implements HttpService {

    private HttpClient httpClient;

    @PostConstruct
    public void activate() {
        httpClient = HttpClientBuilder.create().build();
    }

    @Override
    public HttpResponse get(HttpGet get) throws IOException {
        return httpClient.execute(get);
    }

    @Override
    public HttpResponse post(HttpPost post) throws IOException {
        return httpClient.execute(post);
    }
}
