package com.spiiras.worktimeclient.controller;

import com.spiiras.worktimeclient.services.AttendanceEventService;
import org.apache.http.HttpResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;

/**
 * Created by Igor on 06.05.2017.
 */
@RestController
@RequestMapping(path = "/system")
public class SystemEventsController {

    @Autowired
    private AttendanceEventService attendanceEventService;

    @RequestMapping(path = "/lock", method = RequestMethod.POST)
    public boolean lockSystem(HttpServletRequest request) {
        attendanceEventService.lockSystem();
        System.out.println(LocalDateTime.now());
        return true;
    }

    @RequestMapping(path = "/unlock", method = RequestMethod.POST)
    public boolean unlockSystem(HttpServletRequest request) {
        attendanceEventService.unlockSystem();
        System.out.println(LocalDateTime.now());
        return true;
    }
}
